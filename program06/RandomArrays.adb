   -- Morgan Showman
   -- CS1300
	-- Number Chains
	-- 11/24/2008
	
	-- This program will generate a series of 10,000 random integers
	-- in the range of 1-10000, print out the first and last 100 elements
	-- of the array, calculate and print the mean of the array, the
	-- variance of the array, and the mode(s) of the array
   
	with Ada.Text_IO, Ada.Integer_Text_IO, Ada.Numerics.Discrete_Random;
   use  Ada.Text_IO, Ada.Integer_Text_IO;

    procedure RandomArrays is
   
      last : Integer := 10000;
   
      type RandomNumbers_Type is Array (0..last) of Integer;
      RandomNumbers : RandomNumbers_Type;
      type NumberCount_Type   is Array (0..last) of Integer;
      NumberCount   : NumberCount_Type;
      
      subtype data is Integer range 0..last;
       package rand_int is new Ada.Numerics.Discrete_Random(data);
      seed : rand_int.generator;
      
       Procedure FillArray (sum : in out Integer) is
      
      begin		-- Fill Array
      	-- Fill RandomNumbers Array with random numbers
         RandomNumbers(0) := 0;		-- Set zero-ith number to 0
         NumberCount(0)   := 0;		-- Set zero-ith count to 0
         for i in 1..last loop		-- Process all index values in RandomNumbers
            RandomNumbers(i) := Integer(rand_int.random(seed));		-- Set Random number
            sum := sum + RandomNumbers(i);		-- Increase the sum of all numbers in the array
         end loop;
      end FillArray;
      
       Procedure OutputRows (start,stop : Integer) is
      
      begin		-- Output Rows
         for i in start..stop loop
            Put(RandomNumbers(i),7);
            if (i mod 10 = 0) then		-- if there are 10 numbers in the row
               New_Line;					-- move cursor to the next row
            end if;
         end loop;
         New_Line;
      end OutputRows;
      
       Procedure OutputMean (sum,last : Integer; mean : in out Integer)is
      
      begin		-- Output Mean
         -- Output mean value
         Put("   Mean Value: ");
         mean := sum/last;
         Put(mean,1);
         New_Line(2);
      end OutputMean;
   	      
       Procedure OutputVariance (variance : in out Integer; mean,last : Integer) is
      
      begin		-- OutputVariance
      	-- Output variance
         Put("   Variance: ");
         for i in 1..last loop
         -- Calculate Variance
            variance := (variance + (mean-RandomNumbers(i))*(mean-RandomNumbers(i)))/last;
         end loop;
         Put(variance,1);
         New_Line(2);
      end OutputVariance;
      
       Procedure CalculateModeCount (mode_count : out Integer) is
      
      begin		-- Calculate Mode Count
      
      	-- Find mode_count
         Put("   Mode: ");
         mode_count := 0;
         for i in 1..last loop		-- Process all index values in RandomNumbers
            NumberCount(i) := 0;		-- Set current index value count to 0
            for c in reverse 1..i-1 loop		-- Search for previous appearance
               if (RandomNumbers(i) = RandomNumbers(c)) then	-- if previous appearance found
                  NumberCount(i) := NumberCount(c) + 1;	-- set current count = previous count + 1
                  exit;												-- go to next index value
               end if;
            end loop;
            if (NumberCount(i) = 0) then		-- if previous appearance not found
               NumberCount(i) := 1;				-- set count to 1
            end if;
            if (NumberCount(i) > mode_count) then		-- if count > current mode_count
               mode_count := NumberCount(i);				-- set new mode_count
            end if;
         end loop;
      
      end CalculateModeCount;
      
       Procedure OutputModes (mode_count : Integer) is
      
      begin		-- Output Modes
      
      	-- Output mode(s)
         for i in reverse 1..last loop
            if (NumberCount(i) = mode_count) then
               Put(RandomNumbers(i),7);
            end if;
         end loop;
      
      end OutputModes;
   
      sum, mean, variance, indexofmode, count, mode_count, counter : Integer := 0;
   
   begin		-- RandomArrays
   
   	-- Ouput Header
      New_Line;
      Put_Line("            First and Last 100 Numbers for CS1300 Project 6");
      Put_Line("            ===============================================");
      New_Line;
   
   	-- Initialize random number
      rand_int.reset(seed);
   
      FillArray(sum);
      
      -- Output First 100 numbers in array
      OutputRows(1,100);
      
      -- Output Last 100 numbers in array
      OutputRows(last-99,last);
      
   	-- Output mean value
      OutputMean(sum,last,mean);
      
   	-- Output variance
      OutputVariance(variance,mean,last);
      
   	-- Find mode_count
      CalculateModeCount(mode_count);
      
   	-- Output mode(s)
      OutputModes(mode_count);
      
      New_Line(2);
   
   end RandomArrays;