  	-- Morgan Showman
	-- CS1300
	-- Number Chains
	-- 11/10/2008
	
	-- This program reads a number from the user and outputs
	-- the number chain and the length of that chain for the number
	  
   With Ada.Text_IO, Ada.Integer_Text_IO, screen;

    Procedure NumberChains is
    
       Function sort (UserInput : String; Length : Integer;
       					 Order : String) return String is
      					 
         IndexOfSmallest, Index: integer;
         Number : String (1..Length);
         Temp: Character;
         Current, Index_Loop: Integer := 1;
      
      begin --sort
      	-- Set number to be sorted
         Number := UserInput (UserInput'First..Length);
         Index := 1;
         
      	-- Sort number in order specified
         while Current < Length loop
         	-- Set IndexOfSmallest to first digit
            IndexOfSmallest := Current;
            
         	-- Find next smallest digit in number
            Index_Loop := Current + 1;
            for Index in (Index_Loop)..(Length) loop
               if (Order = "ASC") then
                  if (Number(Index) < Number(IndexOfSmallest))then
                     IndexOfSmallest := Index;
                  end if;
               elsif (Order = "DESC") then
                  if (Number(Index) > Number(IndexOfSmallest))then
                     IndexOfSmallest := Index;
                  end if;
               end if;
            end loop;
            
         	-- Swap the next smallest digit with the current digit
            Temp := Number(IndexOfSmallest);
            Number(IndexOfSmallest) := Number(Current);
            Number(Current) := Temp;
         
         	-- Increment Current Digit
            Current := Current + 1;
         end loop;
         return number;
      end sort;
      
       Procedure StringTemp (Temp : Integer; NewInput :out String; Last : out Integer) is
       
         InputString : String := Integer'Image(Temp);
         
      begin		-- StringTemp
      
      	-- Put Integer into String for new UserInput
         Ada.Integer_Text_IO.Put(NewInput,Temp);
         Last := NewInput'Last;
         
      End StringTemp;
   
      UserInput  : String  (1..10);
      
      type ChainArray is Array (1..1000) of Integer;
      NumberChain : ChainArray;
   
      Last, Output, Index, Number1, Number2, Temp   : Integer;
      DuplicateFound : Integer := 0;
      
   begin		-- NumberChains
   
      Screen.ClearScreen;
      Screen.MoveCursor(1,1);
      
   	-- Prompt User for Input
      Ada.Text_IO.Put("Please enter a positive integer number between 0 - 1000000000: ");
      Ada.Text_IO.Get_Line(UserInput,Last);
      
      Screen.ClearScreen;
      Screen.MoveCursor(1,1);
      
      Output := Integer'Value(UserInput (1..Last));
      
   	-- If output /= 0 output number chain
      If (Output = 0) Then
         Null;
      Else
      	-- Output Original Number
         Ada.Text_IO.Put("Original Number was ");
         Ada.Integer_Text_IO.Put(Output,1);
         Ada.Text_IO.New_Line(2);
         
      	-- Output Number Chain
         Index := 1;
      	NumberChainLoop :
         While DuplicateFound = 0 Loop
         	-- Sort Number DESC
            Number1 := Integer'Value(sort(UserInput,Last,"DESC"));
            
         	-- Sort Number ASC
            Number2 := Integer'Value(sort(UserInput,Last,"ASC"));
            
         	-- Subtract sorted numbers
            Temp := Number1 - Number2;
            
         	-- Output chain link
            Ada.Integer_Text_IO.Put(Number1,1);
            Ada.Text_IO.Put(" - ");
            Ada.Integer_Text_IO.Put(Number2,1);
            Ada.Text_IO.Put(" = ");
            Ada.Integer_Text_IO.Put(Temp,1);
            
         	-- Store Link in Chain Array
            NumberChain(Index) := Temp;
            
         	-- Set new UserInput for Sort
            StringTemp(Temp,UserInput,Last);
            
         	-- Check if number has already appeared in the chain
            For n in 1..Index loop
               If (Index > 1 And Then Index /= n And Then NumberChain(Index) = NumberChain(n)) Then
                  DuplicateFound := 1;
                  exit;
               End If;
            End loop;
            
         	-- Increment Index
            Index := Index + 1;
            
            Ada.Text_IO.New_Line;
         End Loop NumberChainLoop;
         
         Ada.Text_IO.New_Line;
         
      	-- Output Chain Length
         Ada.Text_IO.Put("Chain length ");
         Ada.Integer_Text_IO.Put(Index-1,1);
         
         Ada.Text_IO.New_Line(2);
      End If;
   End NumberChains;