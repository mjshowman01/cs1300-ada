------------------------------------------------------------------------------
--                                                                          --
--                         GNAT RUNTIME COMPONENTS                          --
--                                                                          --
--                              L I S T _ I O                               --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
--                             $ Revision: 1 $                              --
--                                                                          --
------------------------------------------------------------------------------

   with Ada.Text_IO;
   with Ada.Integer_Text_IO;
   with Ada.Numerics.Discrete_Random;
	
    Package BODY List_IO is
   
       Function CompareTo (Item1, Item2 : Integer) return Integer is
      
      begin	-- Compare To
      
         if (Item1 = Item2) then
            return 0;
         elsif (Item1 > Item2) then
            return 1;
         else
            return -1;
         end if;
         
      end CompareTo;
   
       Function IsThere (Item : Integer; Lst : ListType) return Integer is
       
         TempItem : Integer;
         Index    : Integer := 0;
         
      begin	-- Is There
      
         TempItem := Lst.Item(Index);
         while Index < Lst.Length loop
            if (CompareTo(Item, TempItem) = 0) then
               return 1;		-- Found, return true
            else
               Index := Index + 1;
               TempItem := Lst.Item(Index);
            end If;
         end loop;
         return 0;				-- Not Found, return false
         
      end IsThere;
      
       Function Length (Lst : ListType) return Integer is
      
      begin	-- Length
      
         return Lst.Length;
         
      end Length;
      
       Function Random (Rng1, Rng2 : Integer) return Integer is
      
         subtype Data is Integer range Rng1..Rng2;
          package Rand_Int is new Ada.Numerics.Discrete_Random(Data);
         Seed : Rand_Int.Generator;
      
      begin		-- Random
      
         Rand_Int.Reset(Seed);
         return Integer(Rand_Int.Random(Seed));
      
      end Random;
   
       Procedure Initialize (Lst : in out ListType) is
      
      begin	-- Initialize
      
         Lst.Length := 0;
         
      end Initialize;
   
       Procedure ItemBelongs (Lst : ListType; Item : Integer; Where : out Integer) is
      
      begin	-- Find Where Item Belongs
      
      	-- Unsorted
         Where := Lst.Length;
         
      end ItemBelongs;
   
       Procedure Insert (Lst : in out ListType; Item : Integer) is
       
         Where : Integer;
         
      begin	-- Insert
      
         ItemBelongs(Lst, Item, Where);
         Lst.Item(Where) := Item;
         Lst.Length := Lst.Length + 1;
         
      end Insert;
   	
       Procedure FindItem (Lst : ListType; Item : Integer; Index : out Integer) is
       
      begin	-- Find the Item
      
         Index := 0;
         while CompareTo(Item, Lst.Item(Index)) /= 0 loop
            Index := Index + 1;
         end loop;
         
      end FindItem;
   
       Procedure Remove (Lst : in out ListType; Item : Integer) is
       
         Index : Integer;
         
      begin	-- Remove
      
         FindItem(Lst, Item, Index);
         Lst.Length := Lst.Length - 1;
      	-- Remove Item
         for i in Index..Lst.Length-1 loop
            Lst.Item(i) := Lst.Item(i+1);
         end loop;
         
      end Remove;
   
   	-- Print entire list
       Procedure Print (Lst : ListType; Col : Integer) is
      
         ColCount : Integer := 0;
      
      begin		-- Print
      
         for i in 0..Lst.Length-1 loop
            ColCount := ColCount + 1;
            Ada.Integer_Text_IO.Put(Lst.Item(i), 7);
            if (ColCount mod Col = 0) then
               Ada.Text_IO.New_Line;
            end if;
         end loop;
         
      end Print;
      
   	-- Print list in range of numbers starting from 1, not 0
       Procedure Print (Lst : ListType; Col, Rng1, Rng2 : Integer) is
       
         ColCount : Integer := 0;
         
      begin		-- Print
      
         for i in Rng1-1..Rng2-1 loop
            ColCount := ColCount + 1;
            Ada.Integer_Text_IO.Put(Lst.Item(i), 7);
            if (ColCount mod Col = 0) then
               Ada.Text_IO.New_Line;
            end if;
         end loop;
         
      end Print;
   
   	-- Print entire list (to file)
       Procedure Print (File : Ada.Text_IO.File_Type; Lst : ListType; Col : Integer) is
      
         ColCount : Integer := 0;
      
      begin		-- Print
      
         for i in 0..Lst.Length-1 loop
            ColCount := ColCount + 1;
            Ada.Integer_Text_IO.Put(File, Lst.Item(i), 7);
            if (ColCount mod Col = 0) then
               Ada.Text_IO.New_Line(File);
            end if;
         end loop;
         
      end Print;
      
   	-- Print list (to file) in range of numbers starting from 1, not 0
       Procedure Print (File : Ada.Text_IO.File_Type; Lst : ListType; Col, Rng1, Rng2 : Integer) is
       
         ColCount : Integer := 0;
         
      begin		-- Print
      
         for i in Rng1-1..Rng2-1 loop
            ColCount := ColCount + 1;
            Ada.Integer_Text_IO.Put(File, Lst.Item(i), 7);
            if (ColCount mod Col = 0) then
               Ada.Text_IO.New_Line(File);
            end if;
         end loop;
         
      end Print;
      
       Procedure Randomize (Lst : in out ListType; Rng1, Rng2 : Integer) is
      
         subtype Data is Integer range Rng1..Rng2;
          package Rand_Int is new Ada.Numerics.Discrete_Random(Data);
         Seed : Rand_Int.Generator;
      
      begin		-- Randomize
      
         rand_int.reset(seed);
         for i in 0..size-1 loop
            Insert(Lst, Integer(Rand_Int.Random(Seed)));
         end loop;
         
      end Randomize;
      
       Procedure MergeSort (Lst : in out ListType) is
         
          Procedure Merge (Left, Right : in out ListType; Result : out ListType) is
         
         begin		-- Merge
         
            Initialize(Result);
            while Left.Length > 0 and then Right.Length > 0 loop
               if Left.Item(0) <= Right.Item(0) then
                  Insert(Result, Left.Item(0));
                  Remove(Left, Left.Item(0));
               else
                  Insert(Result, Right.Item(0));
                  Remove(Right, Right.Item(0));
               end if;
            end loop;
            while Left.Length > 0 loop
               Insert(Result, Left.Item(0));
               Remove(Left, Left.Item(0));
            end loop;
            while Right.Length > 0 loop
               Insert(Result, Right.Item(0));
               Remove(Right, Right.Item(0));
            end loop;
            
         end Merge;
         
         Middle : Integer;
         Left, Right, Result : ListType;
         
      begin		-- Merge Sort
      
         Initialize(Left);
         Initialize(Right);
         Initialize(Result);
         if (Lst.Length > 1) then
            Middle := Lst.Length/2;
            for i in 0..Middle-1 loop
               Insert(Left, Lst.Item(i));
            end loop;
            for i in Middle..Lst.Length-1 loop
               Insert(Right, Lst.Item(i));
            end loop;
            MergeSort(Left);
            MergeSort(Right);
            Merge(Left, Right, Result);
            Lst := Result;
         end if;
         
      end MergeSort;
      
       Function Search (Lst : ListType; Item : Integer) return Integer is
      
          Procedure BinarySearch (First, Last : Integer; Found : out Integer) is
         
            Middle : Integer;
         
         begin		-- BinarySearch
         
            Middle := (First + Last)/2;
            if (First > Last) then
               Found := -1;
            elsif (Lst.Item(Middle) = Item) then
               Found := Middle;
            elsif (Item < Lst.Item(Middle)) then
               BinarySearch(First, Middle-1, Found);
            else
               BinarySearch(Middle+1, Last, Found);
            end if;
            
         end BinarySearch;
         
         Found : Integer;
      
      begin		-- Search
      
         BinarySearch(Lst.Item'First, Lst.Item'Last, Found);
         return Found;
         
      end Search;
   
   end List_IO;