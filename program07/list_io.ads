------------------------------------------------------------------------------
--                                                                          --
--                         GNAT RUNTIME COMPONENTS                          --
--                                                                          --
--                              L I S T _ I O                               --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
--                             $ Revision: 1 $                              --
--                                                                          --
------------------------------------------------------------------------------

   with Ada.Text_IO;
    generic
    	
    	size : Integer := 100;
    
    Package List_IO is
   
      type ListArray is Array (0..Size-1) of Integer;
   	
      type ListType is
         record
            Length : Integer range 0..Size;
            Item   : ListArray;
         end record;
         
      ListFile : Ada.Text_IO.File_Type;
   
       Function Random (Rng1, Rng2 : Integer) return Integer;
       
       Procedure Initialize (Lst : in out ListType);
       
       Procedure Insert (Lst : in out ListType; Item : Integer);
       
       Procedure Remove (Lst : in out ListType; Item : Integer);
       
       Procedure Print (Lst : ListType; Col : Integer);
       Procedure Print (Lst : ListType; Col, Rng1, Rng2 : Integer);
       
       Procedure Print (File : Ada.Text_IO.File_Type; Lst : ListType; Col : Integer);
       Procedure Print (File : Ada.Text_IO.File_Type; Lst : ListType; Col, Rng1, Rng2 : Integer);
       
       Procedure Randomize (Lst : in out ListType; Rng1, Rng2 : Integer);
       
       Procedure MergeSort (Lst : in out ListType);
       
       Function Search (Lst : ListType; Item : Integer) Return Integer;
   	 
   private
   
       Function CompareTo (Item1, Item2 : Integer) return Integer;
       
       Function IsThere (Item : Integer; Lst : ListType) return Integer;
       
       Function Length (Lst : ListType) return Integer;
       
       Procedure ItemBelongs (Lst : ListType; Item : Integer; Where : out Integer);
       
       Procedure FindItem (Lst : ListType; Item : Integer; Index : out Integer);
       
   end List_IO;