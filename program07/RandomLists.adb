   -- Morgan Showman
   -- CS1300
   -- Lists
   -- 12/10/2008
   
   -- This program implements a list type with a randomly generated length size
   -- in the range of 8_000 to 10_000. The list is filled with random integers
   -- in the range of -8_000 to 8_000. The program outputs the first and last 100
   -- numbers in the list, sorts them, then outputs the first and last 100 numbers
   -- again. The program then prompts the user to search through the list. All output
	-- is stored in List.txt
   
   with Ada.Text_IO, Ada.Integer_Text_IO, List_IO;

    procedure RandomLists is
    
       Procedure GenerateList (Size : Integer) is
      
          package List is new List_IO(Size);
         subtype ListType is List.ListType;
         
         ListFileName : constant string := "List.txt";
         ListFile : Ada.Text_IO.File_Type;
         
          Procedure OutputSize (Lst : ListType) is
         
         begin    -- OutputSize
         
            -- Output List Size
            Ada.Text_IO.New_Line(ListFile);
            Ada.Text_IO.Put(ListFile, "   List Size: ");
            Ada.Integer_Text_IO.Put(ListFile, Lst.Length, 7);
            Ada.Text_IO.New_Line(ListFile);
            Ada.Text_IO.Put(ListFile, "   ==================");
            Ada.Text_IO.New_Line(ListFile, 2); 
         
         end OutputSize;
         
          Procedure OutputFirstLast (Lst : ListType; Number, Col : Integer) is
         
         begin    -- OutputFirstLast
         
            -- Ouput Header
            Ada.Text_IO.Put_Line(ListFile, "                   First and Last 100 Numbers in List       ");
            Ada.Text_IO.Put_Line(ListFile, "            ================================================");
            Ada.Text_IO.New_Line(ListFile);
         
            -- Output first and last numbers in the list
            List.Print(ListFile, Lst, Col, 1, Number);
            Ada.Text_IO.New_Line(ListFile);
            List.Print(ListFile, Lst, Col, Lst.Length-Number, Lst.Length-1);
            Ada.Text_IO.New_Line(ListFile, 2);
         
         end OutputFirstLast;
         
          Procedure UserSearch (Lst : ListType; Rng : Integer) is
          
            Input, Found : Integer;
         
         begin    -- User Search
         
            -- Search List until user wants to exit
            Ada.Text_IO.Put("Search List: (Type ");
            Ada.Integer_Text_IO.Put(Rng+1,1);
            Ada.Text_IO.Put(" to end the program)");
            Ada.Text_IO.New_Line;
            Ada.Integer_Text_IO.Get(Input);
            while Input /= Rng+1 loop
               Found := List.Search(Lst, Input);
               if (Found > -1) then
                  Ada.Integer_Text_IO.Put(Input);
                  Ada.Integer_Text_IO.Put(ListFile, Input);
                  Ada.Text_IO.Put("   -->   Found");
                  Ada.Text_IO.Put(ListFile, "   -->   Found");
                  Ada.Text_IO.New_Line;
                  Ada.Text_IO.New_Line(ListFile);
               else
                  Ada.Integer_Text_IO.Put(Input);
                  Ada.Integer_Text_IO.Put(ListFile, Input);
                  Ada.Text_IO.Put("   -->   Not Found");
                  Ada.Text_IO.Put(ListFile, "   -->   Not Found");
                  Ada.Text_IO.New_Line;
                  Ada.Text_IO.New_Line(ListFile);
               end if;
               Ada.Integer_Text_IO.Get(Input);
            end loop;
         
         end UserSearch;
         
         RandomList : ListType;
         Rng1 : Integer := -8_000;
         Rng2 : Integer :=  8_000;
      
      begin    -- Generate Random List
      
         -- Create Output File
         Ada.Text_IO.Create(File => ListFile, Name => ListFileName);
      
         List.Initialize(RandomList);                 -- Create the list
         List.Randomize(RandomList, Rng1, Rng2);      -- Fill the list with random numbers
         
         -- Output List Size
         OutputSize(RandomList);
         
         -- Output the first and last 100 numbers in the list
         OutputFirstLast(RandomList, 100, 10);
         
         -- Sort List
         Ada.Text_IO.Put(ListFile, "   Sorting List....");
         List.MergeSort(RandomList);
         Ada.Text_IO.New_Line(ListFile, 3);
         
         -- Output the first and last 100 numbers in the list
         OutputFirstLast(RandomList, 100, 10);
         
         -- Search List until user wants to exit
         UserSearch(RandomList, Rng2);
         
         -- Close Output File
         Ada.Text_IO.Close(ListFile);
         
      end GenerateList;
      
       package List is new List_IO;
      ListSize : Integer;
   
   begin    -- Random Lists
   
      ListSize := List.Random(8_000, 10_000);   -- Generate a random number to size the list
      GenerateList(ListSize);                   -- Create list with a maximum size of "ListSize"
   
   end RandomLists;