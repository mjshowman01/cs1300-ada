with Ada.Text_IO;
with Ada.Integer_Text_IO;

Procedure Graduation is

	tenths, seconds, minutes, hours, days, years, gTenths, gSeconds, gMinutes,
	gHours, gDays, gYears, totalTenths, totalSeconds, totalMinutes, totalHours,
	totalDays, totalYears, graduationSeconds, graduateSeconds 	:	Integer;

begin		-- Graduation
	-- Recieve input from user
	Ada.Text_IO.Put (Item => "Enter an integer number for the tenths of seconds until graduation: ");
	Ada.Integer_Text_IO.Get (Item => tenths);
	Ada.Text_IO.New_Line (Spacing => 2);
	Ada.Text_IO.Put (Item => "Enter and integer number for the tenths of seconds in graduate school: ");
	Ada.Integer_Text_IO.Get (Item => gTenths);
	Ada.Text_IO.New_Line (Spacing => 2);
	
	-- Calculate Total Tenths
	totalTenths := tenths + gTenths;
	
	-- Calculate Graduation Seconds
	graduationSeconds := tenths / 10;
	-- Calculate Graduate Seconds
	graduateSeconds := gTenths / 10;
	
	-- Calculate Years, Days, Hours, Minutes, Seconds, Tenths for Graduation
	seconds 				:= tenths / 10;
	graduationSeconds	:= seconds;
	tenths				:= tenths rem 10;
	minutes				:= seconds / 60;
	seconds				:= seconds rem 60;
	hours					:= minutes / 60;
	minutes				:= minutes rem 60;
	days					:= hours / 24;
	hours					:= hours rem 24;
	years					:= days / 365;
	days					:= days rem 365;
	
	-- Calculate Years, Days, Hours, Minutes, Seconds, Tenths for Graduate School
	gSeconds 			:= gTenths / 10;
	graduateSeconds	:= gSeconds;
	gTenths				:= gTenths rem 10;
	gMinutes				:= gSeconds / 60;
	gSeconds				:= gSeconds rem 60;
	gHours				:= gMinutes / 60;
	gMinutes				:= gMinutes rem 60;
	gDays					:= gHours / 24;
	gHours				:= gHours rem 24;
	gYears				:= gDays / 365;
	gDays					:= gDays rem 365;
	
	-- Calculate Total Years, Days, Hours, Minutes, Seconds, Tenths
	totalTenths				:= tenths + gTenths;
	totalSeconds 			:= seconds + gSeconds;
	totalMinutes			:= minutes + gMinutes;
	totalHours				:= hours + gHours;
	totalDays				:= days + gDays;
	totalYears				:= years + gYears;
	
	totalSeconds 			:= totalSeconds + totalTenths / 10;
	totalTenths				:= totalTenths rem 10;
	totalMinutes			:= totalMinutes + totalSeconds / 60;
	totalSeconds			:= totalSeconds rem 60;
	totalHours				:= totalHours + totalMinutes / 60;
	totalMinutes			:= totalMinutes rem 60;
	totalDays				:= totalDays + totalHours / 24;
	totalHours				:= totalHours rem 24;
	totalYears				:= totalYears + totalDays / 365;
	totalDays				:= totalDays rem 365;
	
	-- Output graduation in seconds
	Ada.Text_IO.Put (Item => "graduation in ");
	Ada.Integer_Text_IO.Put (Item		=> graduationSeconds,
									 Width	=> 1);
	Ada.Text_IO.Put (Item => " seconds.");
	Ada.Text_IO.New_Line;
	
	-- Output time gradute school requires in seconds
	Ada.Text_IO.Put (Item => "graduate school requires ");
	Ada.Integer_Text_IO.Put (Item		=> graduateSeconds,
									 Width	=> 1);
	Ada.Text_IO.Put (Item => " seconds.");
	Ada.Text_IO.New_Line (Spacing => 2);
	
	-- Output Time Table
	Ada.Text_IO.Put_Line (Item 		=> "                 Years      Days      Hours      Mins      Secs      Tenths  ");
	Ada.Text_IO.Put_Line (Item			=> "=============================================================================");
	Ada.Text_IO.Put (Item				=> "Graduation         ");
	Ada.Integer_Text_IO.Put (Item		=> years,
									 Width	=> 1);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> days,
									 Width	=> 3);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> hours,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "        ");
	Ada.Integer_Text_IO.Put (Item		=> minutes,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "        ");
	Ada.Integer_Text_IO.Put (Item		=> seconds,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> tenths,
									 Width	=> 1);
	Ada.Text_IO.New_Line;
	Ada.Text_IO.Put (Item				=> "Graduate School    ");
	Ada.Integer_Text_IO.Put (Item		=> gYears,
									 Width	=> 1);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> gDays,
									 Width	=> 3);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> gHours,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "        ");
	Ada.Integer_Text_IO.Put (Item		=> gMinutes,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "        ");
	Ada.Integer_Text_IO.Put (Item		=> gSeconds,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> gTenths,
									 Width	=> 1);
	Ada.Text_IO.New_Line;
	Ada.Text_IO.Put_Line (Item			=> "=============================================================================");
	Ada.Text_IO.Put (Item				=> "Total              ");
	Ada.Integer_Text_IO.Put (Item		=> totalYears,
									 Width	=> 1);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> totalDays,
									 Width	=> 3);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> totalHours,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "        ");
	Ada.Integer_Text_IO.Put (Item		=> totalMinutes,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "        ");
	Ada.Integer_Text_IO.Put (Item		=> totalSeconds,
									 Width	=> 2);
	Ada.Text_IO.Put (Item				=> "         ");
	Ada.Integer_Text_IO.Put (Item		=> totalTenths,
									 Width	=> 1);
	
end Graduation;