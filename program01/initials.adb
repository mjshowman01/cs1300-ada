-- Morgan Showman
-- CS1300
-- Formatted Printing program
-- 9/17/2008

-- Is a simple program to print my initials
-- MJS in large letters on the screen and quit

   With Ada.Text_IO;

    Procedure initials is
   begin -- initials
   	-- Print initials in big letters
      Ada.Text_IO.Put_Line("MM           MM             JJ      SSSS");
      Ada.Text_IO.Put_Line("MMMM       MMMM             JJ    SS    SS");
      Ada.Text_IO.Put_Line("MM MM     MM MM             JJ  SS");
      Ada.Text_IO.Put_Line("MM  MM   MM  MM             JJ  SS");
      Ada.Text_IO.Put_Line("MM   MM MM   MM             JJ    SSSSSS");
      Ada.Text_IO.Put_Line("MM     M     MM             JJ          SS");
      Ada.Text_IO.Put_Line("MM           MM   JJ       JJ           SS");
      Ada.Text_IO.Put_Line("MM           MM     JJ   JJ     SS    SS");
      Ada.Text_IO.Put_Line("MM           MM       JJJ         SSSS");
   End initials;