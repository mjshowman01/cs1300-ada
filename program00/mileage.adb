-- Morgan Showman
-- mjshowman01
-- Example Program (mileage)
-- CS1300
-- Due Date: 9/3/2008

-- Use AdaI/O packages
   with Ada.Text_IO;
   with Ada.Float_Text_IO;
   with Ada.Integer_Text_IO;
	
    procedure Mileage is
   
   -- This program computes miles per gallon given four amounts
   -- for gallons used, and starting and ending mileages
   
   -- Declare Constants for data
   
      Amt1 : constant Float := 11.7; -- Number of gallons for fillup 1
      Amt2 : constant Float := 14.3; -- Number of gallons for fillup 2
      Amt3 : constant Float := 12.2; -- Number of gallons for fillup 3
      Amt4 : constant Float := 8.5; -- Number of gallons for fillup 4
   
      Start_Miles : constant Float := 67_308.0; -- Starting mileage
      End_Miles	: constant Float := 68_750.7; -- Ending mileage
   
   -- Declare Variable to save result
   
      MPG : Integer range 4 .. 100; -- Computed miles per gallon
   
   begin -- Program Mileage
   	-- Calculate miles per gallon
      MPG := Integer((End_Miles - Start_Miles) / (Amt1 + Amt2 + Amt3 + Amt4));
   	
   	-- Print the report
      Ada.Text_IO.Put(Item => "For the gallon amounts: ");
      Ada.Text_IO.New_Line(Spacing => 1);
      Ada.Float_Text_IO.Put(Item => Amt1,
         						 Fore => 4,
         						 Aft => 1,
         						 Exp => 0);
      Ada.Text_IO.Put(Item => ',');
      Ada.Float_Text_IO.Put(Item => Amt2,
         						 Fore => 4,
         						 Aft => 1,
         						 Exp => 0);
      Ada.Text_IO.Put(Item => ',');
      Ada.Float_Text_IO.Put(Item => Amt3,
         						 Fore => 4,
         						 Aft => 1,
         						 Exp => 0);
      Ada.Text_IO.Put(Item => ',');
      Ada.Float_Text_IO.Put(Item => Amt4,
         						 Fore => 4,
         						 Aft => 1,
         						 Exp => 0);
      Ada.Text_IO.New_Line(Spacing => 2);
      Ada.Text_IO.Put(Item => "a starting mileage of ");
      Ada.Float_Text_IO.Put(Item => Start_Miles,
         						 Fore => 6,
         						 Aft => 1,
         						 Exp => 0);
      Ada.Text_IO.New_Line(Spacing => 1);
      Ada.Text_IO.Put(Item => "and an ending mileage of ");
      Ada.Float_Text_IO.Put(Item => End_Miles,
         						 Fore => 6,
         						 Aft => 1,
         						 Exp => 0);
      Ada.Text_IO.New_Line(Spacing => 2);
      Ada.Text_IO.Put(Item => "the mileage per gallon is ");
      Ada.Integer_Text_IO.Put(Item => MPG,
         						 Width => 3);
      Ada.Text_IO.New_Line(Spacing => 1);
   	
   end Mileage;
