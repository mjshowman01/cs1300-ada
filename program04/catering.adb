 	-- Morgan Showman
	-- CS1300
	-- Catering Service
	-- 10/27/2008
	
	-- A basic program to order catering from IIT  
	
	with Ada.Text_IO;
   with Ada.Integer_Text_IO;
   with Ada.Float_Text_IO;
   with Ada.Calendar;
   with screen;

    procedure Catering is
    
    	-- Constant Data
      Child_Rate		: CONSTANT Float := 00.55;
   	
      Adult_Deluxe	: CONSTANT Float := 29.95;
      Adult_Standard : CONSTANT Float := 25.75;
   	
      Child_Deluxe	: CONSTANT Float := 29.95 * Child_Rate;
      Child_Standard	: CONSTANT Float := 25.75 * Child_Rate;
      
      Surcharge_Fee	: CONSTANT Float := 00.09;
      
      Tax_Tip			: CONSTANT Float := 00.27;
    
       procedure Show_Menu is
      
      begin		-- Show Menu
      
      	-- Clear Screen
         screen.ClearScreen;
      
      	-- Display Data
         Ada.Text_IO.Put_Line("########################################");
         Ada.Text_IO.Put_Line("  IIT Catering and Convention Service   ");
         Ada.Text_IO.Put_Line("########################################");
         Ada.Text_IO.New_Line(Spacing => 2);
         Ada.Text_IO.Put_Line("      ###########################       ");
         Ada.Text_IO.Put_Line("                Room Fees               ");
         Ada.Text_IO.Put_Line("      ###########################       ");
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put_Line("        A     B     C     D     E       ");
         Ada.Text_IO.Put_Line("      $89   $99  $109  $129  $149       ");
         Ada.Text_IO.New_Line(Spacing => 2);
         Ada.Text_IO.Put_Line("       #########################        ");
         Ada.Text_IO.Put_Line("              Meal Prices               ");
         Ada.Text_IO.Put_Line("       #########################        ");
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put_Line("                   Adult   Child        ");
         Ada.Text_IO.Put("       Deluxe:    $29.95  $");
         Ada.Float_Text_IO.Put(Child_Deluxe,2,2,0);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("       Standard:  $25.75  $");
         Ada.Float_Text_IO.Put(Child_Standard,2,2,0);
         Ada.Text_IO.New_Line(Spacing => 2);
      
      	-- Display user menu
         Ada.Text_IO.Put_Line("       #########################        ");
         Ada.Text_IO.Put_Line("                  Menu                  ");
         Ada.Text_IO.Put_Line("       #########################        ");
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put_Line("      1 ..... Enter New Order           ");
         Ada.Text_IO.Put_Line("      2 ..... Exit                      ");
         Ada.Text_IO.New_Line(Spacing => 2);
      
      end Show_Menu;
      
      type Meal_type		is (Deluxe, Standard);
      type Room_type		is (A, B, C, D, E);
      type Weekend_type	is (False, True);
   	
       package Meal_IO is new Ada.Text_IO.Enumeration_IO (Enum => Meal_Type);
       package Room_IO is new Ada.Text_IO.Enumeration_IO (Enum => Room_Type);
       package Weekend_IO is new Ada.Text_IO.Enumeration_IO (Enum => Weekend_Type);
   	
      Meal		: Meal_Type;
      Room		: Room_Type;
      Weekend	: Weekend_Type;
   	
       procedure Order_Input (Num_Adults, Num_Children : out Integer;
       								Meal_Price : out Float;
       								Meal : out Meal_Type; Room : out Room_Type;
       								Weekend : out Weekend_Type) is
       								
         Select_Option : Integer;
      
      begin		-- Order Input
      	
         Ada.Text_IO.Put_Line("Number of Adults: ");
         Ada.Integer_Text_IO.Get(Num_Adults);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put_Line("Number of Children: ");
         Ada.Integer_Text_IO.Get(Num_Children);
         Ada.Text_IO.New_Line;
         loop
            Ada.Text_IO.Put_Line("Type of Meal: ");
            Ada.Text_IO.New_Line;
            Ada.Text_IO.Put_Line("1 .... Deluxe");
            Ada.Text_IO.Put_Line("2 .... Standard");
            Ada.Text_IO.New_Line;
            Ada.Integer_Text_IO.Get(Select_Option);
            If (Select_Option = 1) Then
               Meal := Deluxe;
               Meal_Price := Adult_Deluxe;
               exit;
            ElsIf (Select_Option = 2) Then
               Meal := Standard;
               Meal_Price := Adult_Standard;
               exit;
            Else
               Null;
            End If;
         end loop;
         Ada.Text_IO.New_Line;
         loop
            Ada.Text_IO.Put_Line("What Room: ");
            Ada.Text_IO.New_Line;
            Ada.Text_IO.Put_Line("1 .... A");
            Ada.Text_IO.Put_Line("2 .... B");
            Ada.Text_IO.Put_Line("3 .... C");
            Ada.Text_IO.Put_Line("4 .... D");
            Ada.Text_IO.Put_Line("5 .... E");
            Ada.Text_IO.New_Line;
            Ada.Integer_Text_IO.Get(Select_Option);
            Ada.Text_IO.New_Line;
            If (Select_Option = 1) Then
               Room := A;
               exit;
            ElsIf (Select_Option = 2) Then
               Room := B;
               exit;
            ElsIf (Select_Option = 3) Then
               Room := C;
               exit;
            ElsIf (Select_Option = 4) Then
               Room := D;
               exit;
            ElsIf (Select_Option = 5) Then
               Room := E;
               exit;
            Else
               Null;
            End If;
         end loop;
         Ada.Text_IO.New_Line;
         loop
            Ada.Text_IO.Put_Line("Weekend? (True/False):");
            Ada.Text_IO.New_Line;
            Ada.Text_IO.Put_Line("1 .... True");
            Ada.Text_IO.Put_Line("2 .... False");
            Ada.Text_IO.New_Line;
            Ada.Integer_Text_IO.Get(Select_Option);
            Ada.Text_IO.New_Line;
            If (Select_Option = 1) Then
               Weekend := True;
               exit;
            ElsIf (Select_Option = 2) Then
               Weekend := False;
               exit;
            Else
               Null;
            End If;
         end loop;
      
      end Order_Input;
      
       function Calc_Total (Number_Of : Integer;
       							Price : Float) return Float is
      
      begin		-- Calculate Total Price
      
         return float(Number_Of) * Price;
      
      end Calc_Total;
   	
       procedure New_Order (Adult_Total, Child_Total, Sub_Total, Total, Balance, Tax_Tip_Total, Discount, Meal_Price, Weekend_Fee, Discount_Percent : out Float;
         						 Room_Fee, Num_Adults, Num_Children, Original_Room_Fee : out Integer) is
      
      begin		-- New Order
      
      	-- Clear Screen
         screen.ClearScreen;
      
         Order_Input (Num_Adults, Num_Children, Meal_Price, Meal, Room, Weekend);
         
         case Meal is
            when Deluxe =>
            -- Calculate Total of Adult Meals
               Adult_Total := Calc_Total (Num_Adults, Adult_Deluxe);
            -- Calculate Total of child Meals
               Child_Total := Calc_Total (Num_Children, Child_Deluxe);
            when Standard =>
            -- Calculate Total of Adult Meals
               Adult_Total := Calc_Total (Num_Adults, Adult_Standard);
            -- Calculate Total of child Meals
               Child_Total := Calc_Total (Num_Children, Child_Standard);
         end case;
      	-- Add Sub Total
         Sub_Total := Adult_Total + Child_Total;
         
      	-- Determine Room Fee
         case Room is
            when A =>
               Room_Fee := 89;
            when B =>
               Room_Fee := 99;
            when C =>
               Room_Fee := 109;
            when D =>
               Room_Fee := 129;
            when E =>
               Room_Fee := 149;
         end case;
         
         Original_Room_Fee := Room_Fee;
      	
      	-- If weekend
         If (Weekend = True) Then
         	-- add surcharge
            Weekend_Fee		:= Sub_Total * Surcharge_Fee + float(Room_Fee) * Surcharge_Fee;
            Sub_Total		:= Sub_Total * Surcharge_Fee + Sub_Total;
            Room_Fee			:= integer(float(Room_Fee) * Surcharge_Fee + float(Room_Fee));
         End If;
      	-- Calculate tax & tip
         Tax_Tip_Total := Sub_Total * Tax_Tip;
         Sub_Total := Sub_Total + Tax_Tip_Total;
      	-- Calculate total price
         Total := Sub_Total + float(Room_Fee);
      	
      	-- Calc prompt payment balance due 
         If (Total < 100.00) Then
            Discount_Percent := 0.0075;
            Discount := Total * Discount_Percent;
            Balance	:= Total - Discount;
         ElsIf (Total < 200.00) Then
            Discount_Percent := 0.0150;
            Discount := Total * Discount_Percent;
            Balance	:= Total - Discount;
         ElsIf (Total < 400.00) Then
            Discount_Percent := 0.0375;
            Discount := Total * Discount_Percent;
            Balance	:= Total - Discount;
         ElsIf (Total < 800.00) Then
            Discount_Percent := 0.0625;
            Discount := Total * Discount_Percent;
            Balance	:= Total - Discount;
         Else
            Discount_Percent := 0.0700;
            Discount := Total * Discount_Percent;
            Balance	:= Total - Discount;
         End If;
      
      end New_Order;
      
       procedure Print_Order (Adult_Total, Child_Total, Sub_Total, Total, Balance, Tax_Tip_Total, Discount, Meal_Price, Weekend_Fee, Discount_Percent : in out Float;
       							  Room_Fee, Num_Adults, Num_Children, Original_Room_Fee : in out Integer) is
       							  
         Weekend_YN : String := "   ";
         
         Year,Month,Day : Integer;
         Seconds			 : Duration;
         
         Input		 : Character;
         Available : Boolean;
      
      begin		-- Print Order
      
      	-- Clear Screen
         screen.ClearScreen;
      
      	-- Display Bill
         Ada.Text_IO.Put_Line("          IIT Catering and Convention Service         ");
         Ada.Text_IO.Put_Line("                    Final Bill                        ");
         Ada.Text_IO.Put("                    ");
         
      	-- Get Time
         Ada.Calendar.split(Ada.Calendar.Clock,Year,Month,Day,Seconds);
         Ada.Integer_Text_IO.Put(Month,1);
         Ada.Text_IO.Put("/");
         Ada.Integer_Text_IO.Put(Day,1);
         Ada.Text_IO.Put("/");
         Ada.Integer_Text_IO.Put(Year,4);
      	
         Ada.Text_IO.New_Line(Spacing => 2);
         
         Ada.Text_IO.Put("Number of Adults:        ");
         Ada.Integer_Text_IO.Put(Num_Adults,width=>2);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("Number of Children:      ");
         Ada.Integer_Text_IO.Put(Num_Children,width=>2);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("Type of Meal/Price:      ");
         Meal_IO.Put(Meal,width=>8);
         Ada.Text_IO.Put("             $  ");
         Ada.Float_Text_IO.Put(Meal_Price,2,2,0);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("Price of Adult Meals:                         $ ");
         Ada.Float_Text_IO.Put(Adult_Total,3,2,0);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("Price of Child Meals:                         $ ");
         Ada.Float_Text_IO.Put(Child_Total,3,2,0);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("Room Fee:                                     $ ");
         Ada.Integer_Text_IO.Put(Original_Room_Fee,width=>3);
         Ada.Text_IO.Put(".00");
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("Weekend:                 ");
         case Weekend is
            when TRUE =>
               Weekend_YN := "Yes";
            when FALSE =>
               Weekend_YN := " No";
         end case;
         Ada.Text_IO.Put(Weekend_YN);
         Ada.Text_IO.Put("                  $  ");
         Ada.Float_Text_IO.Put(Weekend_Fee,2,2,0);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put("Taxes and Tip:                                $ ");
         Ada.Float_Text_IO.Put(Tax_Tip_Total,3,2,0);
         Ada.Text_IO.New_Line(Spacing => 2);
         Ada.Text_IO.Put("Total Price:        Pay this amount           $ ");
         Ada.Float_Text_IO.Put(Total,3,2,0);
         Ada.Text_IO.New_Line(Spacing => 2);
         Ada.Text_IO.Put("Prompt Payment Discount:          ");
         Ada.Float_Text_IO.Put(Discount_Percent*100.00,1,2,0);
         Ada.Text_IO.Put("%       $  ");
         Ada.Float_Text_IO.Put(Discount,2,2,0);
         Ada.Text_IO.New_Line(Spacing => 2);
         Ada.Text_IO.Put_Line("Balance Due:                                          ");
         Ada.Text_IO.Put("Pay this amount if less than 10 days          $ ");
         Ada.Float_Text_IO.Put(Balance,3,2,0);
         Ada.Text_IO.New_Line(Spacing => 2);
      
      end Print_Order;
      
       function Menu_Option return Integer is
      
         Adult_Total, Child_Total, Sub_Total, Tax_Tip_Total, Total, Discount, Balance, Meal_Price, Weekend_Fee, Discount_Percent : Float;
         Option, Room_Fee, Num_Adults, Num_Children, Original_Room_Fee : Integer;
      
      begin		-- Menu Option
      
         Ada.Text_IO.Put("Please select a menu option: ");
         Ada.Integer_Text_IO.Get(Option);
         
         If (Option = 1) Then
            New_Order(Adult_Total, Child_Total, Sub_Total, Total, Balance, Tax_Tip_Total, Discount, Meal_Price, Weekend_Fee, Discount_Percent,
               		 Room_Fee, Num_Adults, Num_Children, Original_Room_Fee);
            Print_Order(Adult_Total, Child_Total, Sub_Total, Total, Balance, Tax_Tip_Total, Discount, Meal_Price, Weekend_Fee, Discount_Percent,
               			Room_Fee, Num_Adults, Num_Children, Original_Room_Fee);
         ElsIf (Option = 2) Then
            screen.ClearScreen;		-- Exit Program
         Else
            Show_Menu;
            Ada.Text_IO.Put("Sorry that is not a valid option.");
            Ada.Text_IO.New_Line;
            Option := Menu_Option;
         End If;
      
         return Option;
      	
      end Menu_Option;
   
      Option	: Integer;
   
   begin  -- Catering
   	
      Show_Menu;
   
   	-- Promt user for action
      Option := Menu_Option;
   
   end Catering;