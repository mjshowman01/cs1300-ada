	-- Morgan Showman
	-- mjshowman01
	-- Gradtuation Program 3
	-- CS1300
	-- 10/8/2008
	
	-- Program uses procedures to recive input from the user, calculate
	-- data from values given from user, output totals of the calculated
	-- data in a table, then exits.

   with Ada.Text_IO;
   with Ada.Integer_Text_IO;

    Procedure Graduation is
   
       Procedure Get_User_Input (Time_Period	: in	String;		-- Time Period to Calculate
       								   Tenths		: out	Integer) is	-- tenths of second from user
      
      begin		-- Get User Input
      
         Ada.Text_IO.Put (Item => "Enter an integer number for the tenths of seconds until " & Time_Period & ": ");
         Ada.Integer_Text_IO.Get (Item => tenths);
      
      end Get_User_Input;
   
       Procedure Calc_Seconds (Tenths	: in Integer;			-- Calculate Total Seconds for Time Period
       								 Seconds, Total : out Integer) is
      
      begin		-- Calc Seconds
      
         Seconds	:= Tenths / 10;
         Total		:= Seconds;
      
      end Calc_Seconds;
   	
       Procedure Calc_Time_Period (Tenths : in out Integer;			-- Calculate Years, Days, Hours, Minutes, Seconds, Tenths
       									  Seconds, Minutes, Hours, Days,	-- for Time Period
       									  Years : out Integer) is
      
      begin		-- Calc Time Period
      
         Seconds 	:= Tenths / 10;
         Tenths	:= Tenths rem 10;
         Minutes	:= Seconds / 60;
         Seconds	:= Seconds rem 60;
         Hours		:= Minutes / 60;
         Minutes	:= Minutes rem 60;
         Days		:= Hours / 24;
         Hours		:= Hours rem 24;
         Years		:= Days / 365;
         Days		:= Days rem 365;
      
      end Calc_Time_Period;
   
       Procedure Calculate_Data (Tenths : in out Integer;												-- Calculate Data from User Input
       								   Seconds, Minutes, Hours, Days, Years, Total : out	Integer) is
      
      begin		-- Calculate Data
      
      	-- Calculate Time Period Seconds
         Calc_Seconds (Tenths, Seconds, Total);
      	-- Calculate Time Period Years, Days, Hours, Minutes, Seconds, Tenths
         Calc_Time_Period (Tenths, Seconds, Minutes, Hours, Days, Years);
         
      end Calculate_Data;
      
       Procedure Add_Total (Tenths, Seconds, Minutes, Hours, Days, Years : out Integer;				-- Add together data from all Time Periods
       					      gTenths, gSeconds, gMinutes, gHours, gDays, gYears,
       						   gradTenths, gradSeconds, gradMinutes, gradHours, gradDays, gradYears
       				  			: in out Integer) is
      
      begin		-- Add Total
      
         Tenths	:= gradTenths + gTenths;
         Seconds 	:= gradSeconds + gSeconds;
         Minutes	:= gradMinutes + gMinutes;
         Hours		:= gradHours + gHours;
         Days		:= gradDays + gDays;
         Years		:= gradYears + gYears;
      
      end Add_Total;
   	
       Procedure Calc_Total (Tenths, Seconds, Minutes, Hours, Days, Years		-- Calculate Total time over all Time Periods
       							 : in out Integer) is
      
      begin		-- Calc Total
      
         Seconds 	:= Seconds + Tenths / 10;
         Tenths	:= Tenths rem 10;
         Minutes	:= Minutes + Seconds / 60;
         Seconds	:= Seconds rem 60;
         Hours		:= Hours + Minutes / 60;
         Minutes	:= Minutes rem 60;
         Days		:= Days + Hours / 24;
         Hours		:= Hours rem 24;
         Years		:= Years + Days / 365;
         Days		:= Days rem 365;
      
      end Calc_Total;
   
   	
       Procedure Calculate_Total (Tenths, Seconds, Minutes, Hours, Days, Years : out Integer;
       								   gTenths, gSeconds, gMinutes, gHours, gDays, gYears,
       								   gradTenths, gradSeconds, gradMinutes, gradHours, gradDays, gradYears
       								   : in out Integer) is
      
      begin		-- Calculate Total
      
      	-- Calculate Total Years, Days, Hours, Minutes, Seconds, Tenths
         Add_Total (Tenths, Seconds, Minutes, Hours, Days, Years,
            		  gTenths, gSeconds, gMinutes, gHours, gDays, gYears,
            		  gradTenths, gradSeconds, gradMinutes, gradHours, gradDays, gradYears);
         Calc_Total (Tenths, Seconds, Minutes, Hours, Days, Years);
      
      end Calculate_Total;
      
       Procedure Output_Seconds (gTotalSeconds, gradTotalSeconds : in Integer) is
      
      begin		-- Output Seconds
      
      	-- Output graduation in seconds
         Ada.Text_IO.Put (Item => "graduation in ");
         Ada.Integer_Text_IO.Put (Item		=> gTotalSeconds,
            							 Width	=> 1);
         Ada.Text_IO.Put (Item => " seconds.");
         Ada.Text_IO.New_Line;
      	
      	-- Output time gradute school requires in seconds
         Ada.Text_IO.Put (Item => "graduate school requires ");
         Ada.Integer_Text_IO.Put (Item		=> gradTotalSeconds,
            							 Width	=> 1);
         Ada.Text_IO.Put (Item => " seconds.");
         Ada.Text_IO.New_Line (Spacing => 2);
      
      end Output_Seconds;
   
       Procedure Output_Table (gTenths, gSeconds, gMinutes, gHours, gDays, gYears,
       								gradTenths, gradSeconds, gradMinutes, gradHours, gradDays, gradYears,
       								totalTenths, totalSeconds, totalMinutes, totalHours, totalDays, totalYears : in Integer) is
      
      begin		-- Output Table
      
      	-- Output Time Table
         Ada.Text_IO.Put_Line (Item 		=> "                 Years      Days      Hours      Mins      Secs      Tenths  ");
         Ada.Text_IO.Put_Line (Item			=> "=============================================================================");
         Ada.Text_IO.Put (Item				=> "Graduation         ");
         Ada.Integer_Text_IO.Put (Item		=> gYears,
            							 Width	=> 1);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> gDays,
            							 Width	=> 3);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> gHours,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "        ");
         Ada.Integer_Text_IO.Put (Item		=> gMinutes,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "        ");
         Ada.Integer_Text_IO.Put (Item		=> gSeconds,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> gTenths,
            							 Width	=> 1);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put (Item				=> "Graduate School    ");
         Ada.Integer_Text_IO.Put (Item		=> gradYears,
            							 Width	=> 1);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> gradDays,
            							 Width	=> 3);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> gradHours,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "        ");
         Ada.Integer_Text_IO.Put (Item		=> gradMinutes,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "        ");
         Ada.Integer_Text_IO.Put (Item		=> gradSeconds,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> gradTenths,
            							 Width	=> 1);
         Ada.Text_IO.New_Line;
         Ada.Text_IO.Put_Line (Item			=> "=============================================================================");
         Ada.Text_IO.Put (Item				=> "Total              ");
         Ada.Integer_Text_IO.Put (Item		=> totalYears,
            							 Width	=> 1);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> totalDays,
            							 Width	=> 3);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> totalHours,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "        ");
         Ada.Integer_Text_IO.Put (Item		=> totalMinutes,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "        ");
         Ada.Integer_Text_IO.Put (Item		=> totalSeconds,
            							 Width	=> 2);
         Ada.Text_IO.Put (Item				=> "         ");
         Ada.Integer_Text_IO.Put (Item		=> totalTenths,
            							 Width	=> 1);
      
      end Output_Table;
   
       Procedure Output (gTenths, gSeconds, gMinutes, gHours, gDays, gYears,
       						gradTenths, gradSeconds, gradMinutes, gradHours, gradDays, gradYears,
       						totalTenths, totalSeconds, totalMinutes, totalHours, totalDays, totalYears,
       						gTotalSeconds, gradTotalSeconds : in Integer) is
      
      begin		-- Output
      
         Output_Seconds (gTotalSeconds, gradTotalSeconds);
         Output_Table (gTenths, gSeconds, gMinutes, gHours, gDays, gYears,
            			  gradTenths, gradSeconds, gradMinutes, gradHours, gradDays, gradYears,
            			  totalTenths, totalSeconds, totalMinutes, totalHours, totalDays, totalYears);
      
      end Output;
   
   	-- Graduation Declarations
      Time_Period		: String	(1..20);
      gTenths, gSeconds, gMinutes, gHours, gDays, gYears,
      gradTenths, gradSeconds, gradMinutes, gradHours, gradDays, gradYears,
      totalTenths, totalSeconds, totalMinutes, totalHours, totalDays, totalYears,
      gTotalSeconds, gradTotalSeconds
      : Integer;
   
   begin		-- Graduation
   
   -- Recieve Time Period Input From User
      Get_User_Input (Time_Period	=> "Graduation",			-- Get Graduation
         				 Tenths			=> gTenths);				-- Time From User
      Get_User_Input (Time_Period	=> "Graduate School",	-- Get Graduate School
         				 Tenths			=> gradTenths);			-- Time From User
         				 
   	-- Calculate Data	
      Calculate_Data (Tenths					=> gTenths,			-- Calculate
         				 Seconds					=> gSeconds,		-- Graduation
         				 Minutes					=> gMinutes,		-- Data
         				 Hours					=> gHours,
         				 Days						=> gDays,
         				 Years					=> gYears,
         				 Total					=> gTotalSeconds);
      Calculate_Data (Tenths					=> gradTenths,		-- Calculate
         				 Seconds					=> gradSeconds,	-- Graduate
         				 Minutes					=> gradMinutes,	-- School
         				 Hours					=> gradHours,		-- Data
         				 Days						=> gradDays,
         				 Years					=> gradYears,
         				 Total					=> gradTotalSeconds);
      Calculate_Total (Tenths					=> totalTenths,	-- Calculate
         				  Seconds				=> totalSeconds,	-- Total
         				  Minutes				=> totalMinutes,	-- Data
         				  Hours					=> totalHours,
         				  Days					=> totalDays,
         				  Years					=> totalYears,
         				  gTenths				=> gTenths,
         				  gSeconds				=> gSeconds,
         				  gMinutes				=> gMinutes,
         				  gHours					=> gHours,
         				  gDays					=> gDays,
         				  gYears					=> gYears,
         				  gradTenths			=> gradTenths,
         				  gradSeconds			=> gradSeconds,
         				  gradMinutes			=> gradMinutes,
         				  gradHours				=> gradHours,
         				  gradDays				=> gradDays,
         				  gradYears				=> gradYears);
   
   	-- Output Data
      Output (totalTenths						=> totalTenths,	-- Calculate
         	  totalSeconds						=> totalSeconds,	-- Total
         	  totalMinutes						=> totalMinutes,	-- Data
         	  totalHours						=> totalHours,
         	  totalDays							=> totalDays,
         	  totalYears						=> totalYears,
         	  gTenths							=> gTenths,
         	  gSeconds							=> gSeconds,
         	  gMinutes							=> gMinutes,
         	  gHours								=> gHours,
         	  gDays								=> gDays,
         	  gYears								=> gYears,
         	  gradTenths						=> gradTenths,
         	  gradSeconds						=> gradSeconds,
         	  gradMinutes						=> gradMinutes,
         	  gradHours							=> gradHours,
         	  gradDays							=> gradDays,
         	  gradYears							=> gradYears,
         	  gTotalSeconds					=> gTotalSeconds,
         	  gradTotalSeconds				=> gradTotalSeconds);
   		
   end Graduation;